﻿using System.Data;

internal class Program
{
    const string DATA_PATH = "data.txt";
    const char DATA_DELIMITER = '#';
    static string[] fields = { "id", "date", "name", "age", "height", "date_birth", "place_birth" };
    static int[] modeList = { 1, 2, 3 };

    private static void Main(string[] args)
    {       
        int mode = 0;
            
        do
        {
            Console.WriteLine("Input option: 1 - get data. 2 - input new data. 3 - exit");
            if (!int.TryParse(Console.ReadLine(), out mode) || Array.IndexOf(modeList, mode) == -1)
            {
                Console.WriteLine("Incorrect option. Available options: [1,2,3]");
            } else
            {
                switch (mode)
                {
                    case 1:
                        readMode();
                        break;
                    case 2:
                        writeMode();                                              
                        break;
                }
            }
        } while (mode != 3);                          
    }

    private static void readMode()
    {
        Console.WriteLine("Getting dictionary data");
        if (File.Exists(DATA_PATH))
        {
            string[] data = File.ReadAllLines(DATA_PATH);
            foreach (string line in data)
            {
                string[] parsedData = line.Split(DATA_DELIMITER);
                int i = 0;
                foreach (string field in fields)
                {
                    Console.WriteLine($"{field}: {parsedData[i]}");
                    i++;
                }
                Console.WriteLine("");
            }
        }
        else
        {
            Console.WriteLine("Dictionary not exists. Add data via 2 mode");
        }
    }

    private static void writeMode()
    {
        Console.WriteLine("Input record data.");
        string[] data = new string[fields.Length];
        int i = 0;
        foreach (string field in fields)
        {
            Console.WriteLine($"input '{field}'");
            data[i] = Console.ReadLine();
            i++;
        }
        writeToFile(data);
        Console.WriteLine("Data successfully added");
    }

    private static void writeToFile(string[] data)
    {
        string[] lines = new string[1];
        string preparedData = String.Join(DATA_DELIMITER, data);
        lines[0] = preparedData;
        File.AppendAllLines(DATA_PATH, lines);        
    }
}